    //Example of an abstract class that has abstract and non-abstract methods ,constructors 
     abstract class IVehicle{  
        IVehicle(){System.out.println("Constructor  is created");}  
       abstract void mileage();  
       void changeGear(){System.out.println("gear changed");}  
     }  
    //Creating a Child class which inherits Abstract class  
     class Bike extends IVehicle{  
     void mileage(){System.out.println(" Bike milage");}  
     }
     class Car extends IVehicle
     {
        void mileage(){System.out.println("Car milagage..");}   
     }  
    //Creating a Test class which calls abstract and non-abstract methods  
     class TestAbstraction2{  
     public static void main(String args[]){  
        IVehicle obj = new Bike();  
      obj.mileage();  
      obj.changeGear();  
     }  
    }  