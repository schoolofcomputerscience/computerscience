abstract class P{

    public abstract void m2();
    public void  m1(){
        System.out.println(" m1 method "); 
    }
    // public void m2(int i , float k){
    //     System.out.println(" m2 method -->"+i+k); 
    // }
    // public Object m3(Object o){
    //     return o;
    // }
    
}
//non final ---> final  accepted
class C extends P{
    protected void m1(){
        System.out.println(" non fial "); 
    } 
    
    public void m2(){
        System.out.println(" i have modified  abstract"); 
    }
   // STring StringBulider String Buffer
   //Modifiers  public private protected default final static
    public String m3(Object o){
        System.out.println(" i have modified "+o); 
        return o+"";
    }
}
//public ---> protected --> default -->private we cannot reduce the scope in overiding
class D extends C{
    public void  m1(){
        System.out.println(" in d  "); 
    }
    
}
class TestOverr{
    public static void main(String args[]){
        P c = new C();
        C d = new D();
        // P p = new P();
        // p.m1();
        // p.m2();
        c.m1();
        c.m2();
       // c.m3("nnnn");
        //d.m1();
        //c.m2(10,10);
    }
}



