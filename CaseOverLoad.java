class CaseOverLoad
{                                       ///byte short int  float long  double 
//                                                    char
     public void m1(){
         System.out.println("no args");    

     }
     //char gh ='v';
    public int m1(int i){
        System.out.println("int value"+i);
        return i;
    }
    // public double m1(float i){
    //     System.out.println("float value");
    //     return i;
    // }
    public Object m1(Object i){
        System.out.println(" Object "); 
        return i; 
    }
    public String m1(String i){
        System.out.println(" string ");  
        return i;
    }
    public void m1(float i){
        System.out.println("long value");
        //return i;
    }
    public void m1(int i, float g){
        System.out.println("going from int to float");
    }
    public void m1(float i,float k){
        System.out.println("going from  float to float");
    }
    String str = "shhss";
    //var method
    // public void m1(int... i){
    //     System.out.println("var agrr no value");
   // Object 
    //     ;
    // }
    int i  = 0;
    ///int i = new int();
    public static void main(String args[]){
        ///System.out.println("int value");
        CaseOverLoad c = new CaseOverLoad(); 
        c.m1();
        c.m1(new Object());
        c.m1(new String());
        //char aa ='A';
        c.m1('A');
        c.m1(10,10f);
        c.m1(10f,10); 
       // c.m1("vvv");
        //c.m1(10f);
        //c.m1("bsc");
         //c.m1(10l);    ///auoto promotion
  
        //c.m1(89.00);
        //c.m1(10,10)
    }
}

//var arg method int ...1.5 versions(old versions get the priority)



// class Animal{

// }
// class Monkey extends Animal{

// }
// class Test{
//     public m1(Anmimal obj){
//     System.out.println("varAnimal");
//     }
//     public m1(Monek obj){
//         System.out.println("varMonkey");
//         }
       //Test t = new Test();
       //Animal a = new Animal();
       // t.m1(a);
// } 