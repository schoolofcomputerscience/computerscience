public class DemoExcep {

   public static void main(String args[]) {
      try {
         int a[] = new int[2];
        /// int data=100/0;  
         System.out.println("Access element three :" + a[3]);
      } catch (ArrayIndexOutOfBoundsException e) {
         System.out.println("Exception thrown  :" + e);
      }
      System.out.println("Out of the block");
   }
}