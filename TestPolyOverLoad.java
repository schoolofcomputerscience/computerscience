class TestPolyOverLoad
{

    public void m1(){
        System.out.println("no args");
    }
    public int m1(int i){
        System.out.println("int value");
        return i;
    }
    public float m1(float i){
        System.out.println("float value");
        return i;
    }
    public static void main(String args[]){
        System.out.println("float value");
    }
}


