
Git global setup

git config --global user.name "Vivek Sharma"
git config --global user.email "vivek.sma84@gmail.com"

Create a new repository

git clone https://gitlab.com/schoolofcomputerscience/computerscience.git
cd computerscience
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main

Push an existing folder

cd existing_folder
git init
git remote add origin https://gitlab.com/schoolofcomputerscience/computerscience.git
git add .
git commit -m "Initial commit"
git push -u origin main

Push an existing Git repository

cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/schoolofcomputerscience/computerscience.git
git push -u origin --all
git push -u origin --tags

